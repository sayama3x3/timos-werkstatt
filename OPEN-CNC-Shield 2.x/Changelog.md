# OPEN-CNC-Shield 2.0
## Changelog
### 2.05
- reset pin of BU2506FV is now tied to GND
- added 2k2 resistors in series to analog outputs of BU2506FV. Otherwise it was not possible to use a handwheel directly with the ocs2 pinout